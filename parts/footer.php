<footer id="footer" class="clearfix footer blue--bg">
	<div class="wrap hpad">
		<div class="row flex flex--wrap clearfix">
		<?php if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) : the_row(); ?>	
		 <div class="footer__item fourcol">
		    <h4 class="footer__item--title"><?php echo the_sub_field('title', 'options'); ?></h4>
		    <?php echo the_sub_field('text', 'options'); ?>
		 </div>
		<?php endwhile; endif; ?>
		</div>
	</div>

</footer>

<?php wp_footer(); ?>

<?php if(!is_preview() && get_field('google_analytics', 'options') ) : ?>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
  window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
  ga('create','<?php echo the_field('google_analytics', 'options'); ?>','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>
<?php endif; ?>

</body>
</html>
