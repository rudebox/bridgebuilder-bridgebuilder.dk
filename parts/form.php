<section class="form blue--bg" id="form">
  	<div class="wrap hpad clearfix flex flex--hvalign form__container">
      <h4 class="center"><?php the_field('form_text', 'options'); ?></h4>
  		<div class="form__wrap">
  			<a class="form__close"><i class="fa fa-times" aria-hidden="true"></i></a>
  		</div>
	  	<?php 
	  		//Gravity form multistep form
	 	 	gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, 2, $echo = true ); 
	  	?>
  	</div>
</section>