<?php 
  /**
   * Description: Skills field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

  $bg = get_sub_field('bg');

  if ($bg === 'blue') {
  	$class = 'blue--bg';
  }

  elseif ($bg === 'blue-light') {
    $class = 'blue-light--bg';	
  }

  elseif ($bg === 'gray-light') {
    $class = 'gray-light--bg';	
  }

  //bg position
  $position = get_sub_field('bg_position');
  
  //bg img
  $img = get_sub_field('bg_img');

  //anchor point
   $anchor = get_sub_field('anchor_id');
  ?>

  <section id="<?php echo $anchor; ?>" class="skills padding--both <?php echo $class; ?> <?php echo $position; ?>" style="background-image: url(<?php echo $img['url']; ?>);"> 
  	<div class="wrap hpad clearfix">
  		<h2 class="center"><?php echo the_sub_field('heading'); ?></h2>
  		<p class="center"><?php echo the_sub_field('text'); ?></p>

  		<div class="row flex flex--wrap">
  			<?php if (have_rows('skill') ) : while(have_rows('skill') ) : the_row(); 
  					$icon = get_sub_field('skill_icon');
  					$title = get_sub_field('icon_title');
  					$text = get_sub_field('icon_text');
            $text = get_sub_field('icon_text');

            //link choice
            $link_selector = get_sub_field('link');
            $link = get_sub_field('trigger_url');
  			?>
        
        
        <?php if ($link_selector === 'yes') {
            echo '<a href="' . $link . '" class="fourcol skills__item center skills__item--trigger">';
              if ($icon) : 
              echo $icon;
              echo '<h6 class="skills__title">' . $title . '</h6>';
              else :
              echo '<h6 class="skills__title skills__title--no-icon">' . $title . '</h6>';
              endif;
              echo $text;
            echo '</a>';
          }

          elseif ($link_selector === 'no') {
            echo '<div class="fourcol skills__item center">';
              if ($icon) :
              echo $icon;
              echo '<h6 class="skills__title">' . $title . '</h6>';
              else: 
              echo '<h6 class="skills__title skills__title--no-icon">' . $title . '</h6>';
              endif;
              echo $text;
            echo '</div>';
          }
        ?>
  	 <?php endwhile; endif; ?>
    
    <?php $title = get_sub_field('cta_title'); ?>
    <?php if (have_rows('cta_links') ) : ?> 			  			
			<div class="skills__cta center eightcol twocol-offset">
				<h3 class="center skills__cta--title"><?php echo $title; ?></h3>

	  			<?php while (have_rows('cta_links') ) : the_row(); 
	  				  	//selector
	  					$type = get_sub_field('link_type');

	  					//normal link
	  					$link = get_sub_field('link');
	  					$link_text = get_sub_field('link_text');

						//phone link
	  					$phone_link = get_sub_field('phone_link');

	  					//mail link  					
	  					$mail_link = get_sub_field('mail_link');
	  			?>

	  			<?php 
	  				if ($type === 'default') {
	  					echo '<a class="btn btn--hollow skills__btn" href="' . $link . '">' . $link_text . '</a>';
	  				}

	  				elseif ($type === 'phone') {
	  					echo '<a class="btn btn--hollow skills__btn" href="tel:' . get_formatted_phone($phone_link) . '">' . $phone_link . '</a>';
	  				}

	  				elseif ($type === 'mail') {
	  					echo '<a class="btn btn--hollow skills__btn" href="mailto:' . $mail_link . '">' . $mail_link . '</a>';
	  				}
	  			 ?>

	  			<?php endwhile; ?> 
  			</div>
        <?php endif; ?>
  			
  		</div>
  	</div>
  </section>