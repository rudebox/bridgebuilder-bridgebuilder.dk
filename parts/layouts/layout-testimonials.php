<?php 
  /**
   * Description: Testimonials repeater field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */


  $bg = get_sub_field('bg');

  if ($bg === 'blue') {
  	$class = 'blue--bg';
  }

  elseif ($bg === 'blue-light') {
    $class = 'blue-light--bg';	
  }

  elseif ($bg === 'gray-light') {
    $class = 'gray-light--bg';	
  }

  $title = get_sub_field('header');

  //anchor point
  $anchor = get_sub_field('anchor_id');

  //bg position
  $position = get_sub_field('bg_position');
  
  //bg img
  $img = get_sub_field('bg_img');
  ?>


<?php if (have_rows('testimonial') ) : ?>
  <section id="<?php echo $anchor; ?>" class="testimonials padding--both <?php echo $class; ?> <?php echo $position; ?>" style="background-image: url(<?php echo $img['url']; ?>);">
  	<div class="wrap hpad clearfix">
  		<h2 class="center testimonials__heading"><?php echo $title; ?></h2>
  		<div class="row">
  			<?php while (have_rows('testimonial') ) : the_row(); 
  				$company = get_sub_field('company');
  				$quote = get_sub_field('quote');
  			?>

  			<div class="sixcol testimonials__item">
  				<!-- <i class="fas fa-quote-right"></i> -->
  				<h6 class="testimonials__title"><?php echo $company; ?></h6>
  				<p class="testimonials__quote"><?php echo $quote; ?></p>
  			</div>
  			<?php endwhile; ?>
  		</div>
  	</div>
  </section>
  <?php endif; ?>