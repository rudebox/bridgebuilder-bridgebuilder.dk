<?php 
  /**
   * Description: Steps repeater field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */


    if (have_rows('step') ) :
 ?>

<section class="steps padding--both gray-light--bg">
  <div class="wrap hpad clearfix">
    <div class="row">
      <?php while (have_rows('step') ) : the_row(); 
          $title = get_sub_field('step_title');
          $text = get_sub_field('step_text');
      ?>
        <div class="fourcol steps__item">
          <i class="steps__icon"><span>0<?php echo get_row_index(); ?></span></i>
          <h6 class="steps__title"><?php echo $title; ?></h6>
          <p class="steps__text"><?php echo $text; ?></p>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<?php endif; ?>