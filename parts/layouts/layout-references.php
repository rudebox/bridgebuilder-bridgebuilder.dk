<?php 
  /**
   * Description: References repeater field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

    $bg = get_sub_field('bg');

  if ($bg === 'blue') {
  	$class = 'blue--bg';
  }

  elseif ($bg === 'blue-light') {
    $class = 'blue-light--bg';	
  }

  elseif ($bg === 'gray-light') {
    $class = 'gray-light--bg';	
  }

  $title = get_sub_field('header');
  ?>

 <?php if(have_rows('reference') ) : ?>

  <section class="references padding--both <?php echo $class; ?>">
  	<div class="wrap hpad clearfix">
  		<h2 class="center"><?php echo $title; ?></h2>
  		<div class="row">
  			<?php 
				while (have_rows('reference') ) : the_row();
				$logo = get_sub_field('logo');
  			 ?>

  			 <div class="twocol references__item">
  			 	<img src="<?php echo $logo['url']; ?>" alt="reference logo">
  			 </div>
  			<?php endwhile; ?>
  		</div>
  	</div>
  </section>
<?php endif; ?>