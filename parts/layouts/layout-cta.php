<?php 
  /**
   * Description: CTA field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

  $bg = get_sub_field('cta_img');
  $link_text = get_sub_field('cta_link_text'); 
  ?>

  <section class="cta" style="background-image: url(<?php echo $bg['url']; ?>);">
    <div class="wrap hpad clearfix">
      <div class="row">
        <div class="twelvecol center">
          <h2 class="cta__title"><?php echo the_sub_field('cta_title'); ?></h2>
          <a class="btn btn--hollow cta__btn"><?php echo $link_text; ?></a>          
        </div>
      </div>
    </div>
  </section>

<section class="form form--contact blue--bg" id="form">
    <div class="wrap hpad clearfix flex flex--hvalign form__container">
      <h4 class="center"><?php the_field('form_text', 'options'); ?></h4>
      <div class="form__wrap">
        <a class="form__close"><i class="fa fa-times" aria-hidden="true"></i></a>
      </div>
       <?php 
        $form_id = get_sub_field('cta_form_id');
       ?>

       <?php if ($form_id) : ?>

        <?php 
          //Gravity form multistep form
        gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, 2, $echo = true ); 
        ?>
      <?php endif; ?>
    </div>
</section>
