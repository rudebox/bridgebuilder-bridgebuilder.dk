<?php global $layout_count; ?>
<?php 
  //margin selector
  $margin = get_sub_field('margin');

  if ($margin === 'both') { 
    $class = 'padding--both';
  }
  elseif ($margin === 'top') { 
    $class = 'padding--top';
  }
  elseif ($margin === 'bottom') { 
    $class = 'padding--bottom';
  }

  //bg color
  $bg = get_sub_field('bg');

  if ($bg === 'gray-light') { 
    $color = 'gray-light--bg';
  }
  elseif ($bg === 'blue') { 
    $color = 'blue--bg';
  }
  elseif ($bg === 'blue-light') { 
    $color = 'blue-light--bg';
  }

  //bg position
  $position = get_sub_field('bg_position');
  
  //bg img
  $img = get_sub_field('bg_img');

  $anchor = get_sub_field('anchor_id');
?>
<section id="<?php echo $anchor; ?>" class="wysiwygs wysiwygs--<?php echo $layout_count; ?> <?php echo $color; ?> <?php echo $class; ?> <?php echo $position; ?>" style="background-image: url(<?php echo $img['url']; ?>);">
  <div class="wrap hpad clearfix">
    <?php if(get_sub_field('header')): ?>
      <h2 class="center"><?php the_sub_field('header'); ?></h2>
    <?php endif; ?>
    <?php
    if(get_sub_field('offset') !== 'Flexible') {
        $flex = false;
        if(get_sub_field('offset') === '2 to 1') {
          $offset = '2:1';
        } else {
          $offset = '1:2';
        }
      } else {
        $flex = true;
        $offset = null;
      }
      scratch_layout_declare(get_sub_field('wysiwygs'), 2, $flex, $offset);
      while(has_sub_field('wysiwygs')) {
        scratch_layout_start();
          the_sub_field('wysiwyg');
        scratch_layout_end();
      }
    ?>
  </div>
</section>
