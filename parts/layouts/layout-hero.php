<?php 
  /**
   * Description: Hero field group layout
   *
   * @package BridgeBuilder
   * @subpackage bridgebuilder.dk
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

  $img = get_sub_field('image');
  $title = get_sub_field('header');
  $tag = get_sub_field('tag');
  $text = get_sub_field('text');
  $link_text = get_sub_field('link_text');
  $link = get_sub_field('link');
 ?>

<?php if (is_front_page ()) : ?>
 <section class="hero" style="background-image: url(<?php echo $img['url']; ?>);">
<?php else : ?>
 <section class="hero hero--subpage" style="background-image: url(<?php echo $img['url']; ?>);">
<?php endif; ?>
   <div class="wrap hpad clearfix">
     <div class="row">
       <div class="eightcol twocol-offset hero__text">
         <<?php echo $tag; ?> class="hero__title"><?php echo $title; ?></h1>
         <p><?php echo $text; ?></p>
         <?php if ($link && $link_text) : ?>
         <a href="<?php echo $link; ?>" class="hero__link btn btn--hollow btn--form"><?php echo $link_text; ?></a> 
         <?php endif; ?>
       </div>
     </div>
   </div>
 </section>