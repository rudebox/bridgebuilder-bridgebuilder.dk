<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post(); ?>

<main>

	<?php get_template_part('parts/content', 'layouts'); ?>
	<?php get_template_part('parts/form'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
