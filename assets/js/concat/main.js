// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
;  (function($) {

    /*
     *  render_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param $el (jQuery element)
     *  @return  n/a
     */

    function render_map($el) {

      // var
      var $markers = $el.find('.marker');

      // vars
      var args = {
        zoom: 16,
        center: new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      // create map           
      var map = new google.maps.Map($el[0], args);

      // add a markers reference
      map.markers = [];

      // add markers
      $markers.each(function() {

        add_marker($(this), map);

      });

      // center map
      center_map(map);

    }

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param $marker (jQuery element)
     *  @param map (Google Map object)
     *  @return  n/a
     */

    function add_marker($marker, map) {

      // var
      var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

      // create marker
      var marker = new google.maps.Marker({
        position: latlng,
        map: map
      });

      // add to array
      map.markers.push(marker);

      // if marker contains HTML, add it to an infoWindow
      if ($marker.html()) {
        // create info window
        var infowindow = new google.maps.InfoWindow({
          content: $marker.html()
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, 'click', function() {

          infowindow.open(map, marker);

        });
      }

    }

    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param map (Google Map object)
     *  @return  n/a
     */

    function center_map(map) {

      // vars
      var bounds = new google.maps.LatLngBounds();

      // loop through all markers and create bounds
      $.each(map.markers, function(i, marker) {

        var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

        bounds.extend(latlng);

      });

      // only 1 marker?
      if (map.markers.length == 1) {
        // set center of map
        map.setCenter(bounds.getCenter());
        map.setZoom(16);
      } else {
        // fit to bounds
        map.fitBounds(bounds);
      }

    }

    /*
     *  document ready
     *
     *  This function will render each map when the document is ready (page has loaded)
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 5.0.0
     *
     *  @param n/a
     *  @return  n/a
     */

    $(document).ready(function() {

      $('.acf-map').each(function() {

        render_map($(this));

      });

    });

  })(jQuery);;jQuery(document).ready(function($) {

  //ative class pagelink
  $(".nav__item").each(function() {   
      if (this.href == window.location.href) { 
          $(this).addClass("is-active");
      }
  });

  //apply button style to last element
  $('.nav__item').last().addClass('btn btn--hollow btn--nav');
  $('.nav__item').last().removeClass('nav__item');



  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //show form
  $('.btn--form, .btn--nav').click(function() {
    // $('.form').addClass('is-visible');
    // $('body').addClass('is-fixed');
  });


  //show form after dynamic population of checkbox
  if (window.location.href.indexOf("?ydelser") > -1) {
       $('body').addClass('is-fixed');
       $('.form').addClass('is-visible');
    }

 //close form
  $('.form__close').click(function() {
    $('.form').removeClass('is-visible');
    $('body').removeClass('is-fixed');
  });


//sticky icky header
(function() {
    var $header = $('.header');
    var $headerHeight = $header.outerHeight();

    $(window).on('load resize scroll', function(e) {
      var scrollTop = $(window).scrollTop();

      if ( scrollTop >= $headerHeight ) {
        $header.addClass('is-sticky');
      } else {
        $header.removeClass('is-sticky');
      }
    });

  })();

});