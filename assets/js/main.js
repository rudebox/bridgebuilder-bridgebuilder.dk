jQuery(document).ready(function($) {

  //ative class pagelink
  $(".nav__item").each(function() {   
      if (this.href == window.location.href) { 
          $(this).addClass("is-active");
      }
  });

  //apply button style to last element
  $('.nav__item').last().addClass('btn btn--hollow btn--nav');
  $('.nav__item').last().removeClass('nav__item');


  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //show form
  $('.btn--form, .btn--nav').click(function() {
    // $('.form').addClass('is-visible');
    // $('body').addClass('is-fixed');
  });


  //show form after dynamic population of checkbox
  if (window.location.href.indexOf("?ydelser") > -1) {
       $('body').addClass('is-fixed');
       $('.form').addClass('is-visible');
    }

 //close form
  $('.form__close').click(function() {
    $('.form').removeClass('is-visible');
    $('body').removeClass('is-fixed');
  });


//sticky icky header
(function() {
    var $header = $('.header');
    var $headerHeight = $header.outerHeight();

    $(window).on('load resize scroll', function(e) {
      var scrollTop = $(window).scrollTop();

      if ( scrollTop >= $headerHeight ) {
        $header.addClass('is-sticky');
      } else {
        $header.removeClass('is-sticky');
      }
    });

  })();

});