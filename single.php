<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <section class="single">
    <div class="wrap hpad clearfix">

      <article id="post-<?php the_ID(); ?>"
               <?php post_class(); ?>
               itemscope itemtype="http://schema.org/BlogPosting">

        <header>
          <h1 itemprop="headline">
            <?php the_title(); ?>
          </h1>

          <div class="flex flex--wrap flex--justify flex--center single__meta">
            <div><time datetime="<?php the_time('c'); ?>">Udgivet: <span><?php the_time('d.m.Y'); ?></span></time></div> 

            <div>Forfatter: <span><?php echo get_the_author(); ?></span></div>
          </div>
        </header>

        <div itemprop="articleBody">
          <?php the_content(); ?>
        </div>

      </article>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>