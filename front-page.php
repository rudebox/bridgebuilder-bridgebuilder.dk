<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/content', 'layouts'); ?>
  <?php get_template_part('parts/form'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
