<?php get_template_part('parts/header'); ?>

<main>

  <section class="404 padding--both blue--bg">
  	<div class="wrap hpad clearfix">

    	<h1>404</h1>
    	<p>Vi kunne ikke finde siden du søgte efter</p>
    	<a class="btn btn--hollow" href="/">Tilbage</a>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>